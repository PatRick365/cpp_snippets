#pragma once

#include <random>


class QuickRand
{
public:
    template<typename T, typename RandomEngine = std::mt19937>
    static typename std::enable_if<std::is_integral<T>::value, T>::type get(T floor, T ceil)
    {
        std::uniform_int_distribution<T> dist(floor, ceil);
        return dist(randomEngine<RandomEngine>());
    }

    template<typename T, typename RandomEngine = std::mt19937>
    static typename std::enable_if<std::is_floating_point<T>::value, T>::type get(T floor, T ceil)
    {
        std::uniform_real_distribution<T> dist(floor, ceil);
        return dist(randomEngine<RandomEngine>());
    }

private:
    template<typename T>
    static T &randomEngine()
    {
        static std::random_device randomDevice;
        static T randomEngine(randomDevice());

        return randomEngine;
    }
};
