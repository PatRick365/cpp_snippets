class Singleton
{
private:
    Singleton() = default;

public:
    Singleton(const Singleton &) = delete;
    Singleton &operator=(const Singleton &) = delete;
    Singleton(Singleton &&) = delete;
    Singleton &operator=(Singleton &&) = delete;

    static Singleton &get()
    {
        static Singleton instance;
        return instance;
    }

private:
}; 
